CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -g
LDFLAGS = -Wl,--version-script=libbrilldata.v -shared -lm -llapacke

TARGET = libbrilldata.so

CC = cc

OBJS = basis.o               \
       cpu.o                 \
       cpuid.o               \
       eval.o                \
       eval_metric.o         \
       eval_psi_radial.o     \
       expansion.o           \
       init.o                \
       log.o                 \
       qfunc.o               \
       solve.o               \
       threadpool.o          \


all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS) ${LDFLAGS}

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

%.o: %.asm
	nasm -f elf64 -M $< > $(@:.o=.d)
	nasm -f elf64 -o $@ $<

clean:
	-rm -f *.o *.d $(TARGET)

test: $(TARGET)
	LD_LIBRARY_PATH=. PYTHONPATH=. ./tests/ham_constraint.py

-include $(OBJS:.o=.d)

.PHONY: clean test
