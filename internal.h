/*
 * Copyright 2014-2015 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRILL_DATA_INTERNAL_H
#define BRILL_DATA_INTERNAL_H

#include <stddef.h>

#include "brill_data.h"

#include "basis.h"
#include "qfunc.h"
#include "threadpool.h"

#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define SQR(x) ((x) * (x))

#define ARRAY_ELEMS(x) (sizeof(x) / sizeof(x[0]))
#define ALIGN(x, a) (((x)+(a)-1)&~((a)-1))

#define REQ_ALIGNMENT(x) (32 / sizeof(x))

/*
 * small number to avoid r=0 singularities
 */
#define EPS 1E-08

enum CoordSystem {
    COORD_SYSTEM_CYLINDRICAL,
    COORD_SYSTEM_RADIAL,
};

typedef struct BDPriv {
    int coord_system;

    BasisSetContext *basis[2];
    QFuncContext    *qfunc;

    int nb_colloc_points[2];
    int nb_coeffs[2];

    double *coeffs;
    ptrdiff_t coeffs_stride;

    int cpu_flags;

    double (*scalarproduct_metric)(size_t len1, size_t len2, double *mat,
                                   double *vec1, double *vec2);
} BDPriv;

#define NB_COEFFS(s)        (s->nb_coeffs[0]        * s->nb_coeffs[1])
#define NB_COLLOC_POINTS(s) (s->nb_colloc_points[0] * s->nb_colloc_points[1])



int bdi_solve(BDContext *bd);

void bdi_eval_metric(const BDContext *bd,
                     const double *rho, int nb_coords_rho,
                     const double *z,   int nb_coords_z,
                     enum BDMetricComponent comp,
                     const unsigned int diff_order[2],
                     double *psi[9], double *q[9],
                     double *out, ptrdiff_t out_stride);

int bdi_eval_psi_radial(const BDContext *bd,
                        const double *rho, int nb_coords_rho,
                        const double *z,   int nb_coords_z,
                        const unsigned int diff_order[2],
                        double *psi, ptrdiff_t psi_stride);

void bdi_log(const BDContext *bd, int level, const char *fmt, ...);
void bdi_log_default_callback(const BDContext *bd, int level,
                              const char *fmt, va_list vl);

#endif // BRILL_DATA_INTERNAL_H
