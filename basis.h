/*
 * Copyright 2015 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRILL_DATA_BASIS_H
#define BRILL_DATA_BASIS_H

enum BSEvalType {
    BS_EVAL_TYPE_VALUE,
    BS_EVAL_TYPE_DIFF1,
    BS_EVAL_TYPE_DIFF2,
};

enum BasisFamily {
    BASIS_FAMILY_SB_EVEN,
    BASIS_FAMILY_COS_EVEN,
};

typedef struct BasisSetContext BasisSetContext;

BasisSetContext *bdi_basis_init(enum BasisFamily family, double sf);
void bdi_basis_free(BasisSetContext **s);

double bdi_basis_eval(BasisSetContext *s, enum BSEvalType type,
                      double coord, int order);
double bdi_basis_eval_multi(BasisSetContext *s, double coord, double **out,
                            int order_start, int order_end);
double bdi_basis_colloc_point(BasisSetContext *s, int idx, int order);

#endif /* BRILL_DATA_BASIS_H */
