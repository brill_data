/*
 * Copyright 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BRILL_DATA_QFUNC_H
#define BRILL_DATA_QFUNC_H

#include "brill_data.h"

typedef struct QFuncContext QFuncContext;

int bdi_qfunc_init(const BDContext *bd, QFuncContext **out, enum BDQFuncType type,
                   double amplitude, int n, double rho0);
void bdi_qfunc_free(QFuncContext **ps);

double bdi_qfunc_eval(QFuncContext *s, double rho, double z, int diff_idx);

#endif /* BRILL_DATA_QFUNC_H */
