/*
 * Copyright 2014-2015 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * logging code
 */

#include <stdarg.h>
#include <stdio.h>

#include "brill_data.h"
#include "internal.h"

void bdi_log_default_callback(const BDContext *bd, int level,
                              const char *fmt, va_list vl)
{
    vfprintf(stderr, fmt, vl);
}

void bdi_log(const BDContext *bd, int level, const char *fmt, ...)
{
    va_list vl;
    va_start(vl, fmt);
    bd->log_callback(bd, level, fmt, vl);
    va_end(vl);
}
